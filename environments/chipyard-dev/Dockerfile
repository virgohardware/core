FROM archlinux

# This Dockerfile was created for the Tendermint Hardware project and funded by Tendermint, Inc.
# It is focused on speed and ease of use, so it uses pre-compiled tools availalbe in Arch Linux.
# This Dockerfile can be used in CI systems like the one found here: https://gitlab.com/virgohardware/core
# Individual developers can also use this Dockerfile to experiment with Chipyard without having to worry about configuring a development environment.
MAINTAINER jacobgadikian@gmail.com

# Chipyard dependencies recommended by docs: https://chipyard.readthedocs.io/en/latest/Chipyard-Basics/Initial-Repo-Setup.html
RUN pacman -Syyu --noconfirm base-devel bison flex gmp mpfr mpc zlib vim sbt scala texinfo gengetopt expat libusb ncurses cmake python patch diffstat texi2html texinfo subversion chrpath git wget gtk3 dtc rsync libguestfs expat ctags verilator jre11-openjdk spike

RUN archlinux-java set java-11-openjdk

# Clone & init Chipyard
# Chose to keep this step to spare the end user the time burden of cloning all of the submodules.
# Image should be built daily to keep pace with upstream
# Running Make here prepares files needed for tests as described here: https://chipyard.readthedocs.io/en/latest/Simulation/Software-RTL-Simulation.html
RUN git clone https://github.com/ucb-bar/chipyard.git && \
       cd chipyard && \
       ./scripts/init-submodules-no-riscv-tools.sh